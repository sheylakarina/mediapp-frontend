import { TOKEN_NAME } from './../../_shared/var.constant';
import { Component, OnInit } from '@angular/core';
import * as decode from 'jwt-decode';
import {MatInputModule,MatFormFieldControl} from '@angular/material';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  usuario: string;
  rol: string;

  constructor() { }

  ngOnInit() {
    let token= JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken= decode(token.access_token);
    this.usuario= decodedToken.user_name;
    this.rol= decodedToken.authorities[0];
  }

}
