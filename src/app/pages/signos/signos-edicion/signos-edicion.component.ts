import { PacienteService } from './../../../_service/paciente.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SignosService } from '../../../_service/signosService';
import { Signos } from '../../../_model/signos';
import { Paciente } from '../../../_model/paciente';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {
  id: number;
  signos : Signos;
  form: FormGroup;
  edicion: boolean= false;

  myControl: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  
  pacienteSeleccionado: Paciente;

  filteredOptions: Observable<any[]>;

  constructor(private signosService: SignosService,
              private pacienteService: PacienteService,
              private route: ActivatedRoute,
              private router: Router) { 

    this.signos= new Signos();
    this.form= new FormGroup({
      'id': new FormControl(0),
      'fecha' : new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
      'temperatura': new FormControl('')

    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion= params['id']!=null;
      this.initForm();
    })

    this.listarPacientes();

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(null),
        map(val => this.filter(val))
      );

  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {      
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  private initForm(){
    if(this.edicion){
      this.signosService.getSignosPorId(this.id).subscribe(data=>{
        let id=data.idSignos;
        let fecha=data.fecha;
        let pulso=data.pulso;
        let ritmo=data.ritmoRespiratorio;
        let temperatura=data.temperatura;
        //let idPaciente=data.idPaciente;

        this.form= new FormGroup({
          'id': new FormControl(id),
          'fecha' : new FormControl(fecha),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmo),
          'temperatura': new FormControl(temperatura)
          //'idPaciente': new FormControl(idPaciente)
        });
      });
    }
  }

  listarPacientes() {
    this.pacienteService.getlistar().subscribe(data => {
      this.pacientes = data;
    });
  }

  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }

  operar() {
    this.signos.idSignos = this.form.value['id'];
    this.signos.fecha = this.form.value['fecha'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmoRespiratorio = this.form.value['ritmo'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.paciente =  this.pacienteSeleccionado;

    console.log("sigonos "+JSON.stringify(this.signos));

    if (this.signos != null && this.signos.idSignos > 0) {
      this.signosService.modificar(this.signos).subscribe(data => {
        if (data === 1) {
          this.signosService.getListarSignos().subscribe(especialidad => {
            this.signosService.signosCambio.next(especialidad);
            this.signosService.mensaje.next("Se modifico");
          });
        } else {
          this.signosService.mensaje.next("No se pudo modificar");
        }
      });
    } else {
      this.signosService.registrarSignos(this.signos).subscribe(data => {
        if (data === 1) {
          this.signosService.getListarSignos().subscribe(especialidad => {
            this.signosService.signosCambio.next(especialidad);
            this.signosService.mensaje.next("Se registro");
          });
        } else {
          this.signosService.mensaje.next("No se pudo registrar");
        }
      });
    }

    this.signosService.getListarSignos().subscribe(data => {
      this.signosService.signosCambio.next(data);
    });

    this.router.navigate(['signos']);
  }

}
