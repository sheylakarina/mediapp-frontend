import { Paciente } from "./paciente";

export class Signos{
    public idSignos: number;
    public fecha: string;    
    public pulso: string;    
    public ritmoRespiratorio: string;
    public temperatura: string;
    public paciente: Paciente;
}