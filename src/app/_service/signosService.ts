import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Signos } from '../_model/signos';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';



@Injectable()
export class SignosService{
    private url: string= `${HOST}`;
    signosCambio= new Subject<Signos[]>();
    mensaje = new Subject<string>();

    constructor(private http: HttpClient){}

    getListarSignos(){
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.get<Signos[]>(`${this.url}/signos/listar`, {
          headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    getSignosPorId(id: number) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.get<Signos>(`${this.url}/signos/listar/${id}`, {
          headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
      }

      registrarSignos(signos: Signos) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.post(`${this.url}/signos/registrar`, signos, {
          headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
      }

      modificar(signos: Signos) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.put(`${this.url}/signos/actualizar`, signos, {
          headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
      }
}